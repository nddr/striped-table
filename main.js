document.addEventListener('DOMContentLoaded', function (e) {
  const table = document.querySelector('#striped tbody');
  const data = [
    {
      abbr: 'DC',
      deliveryTime: '48 hrs'
    },
    {
      abbr: 'ME',
      deliveryTime: '72 hrs'
    },
    {
      abbr: 'MD',
      deliveryTime: '48 hrs'
    },
    {
      abbr: 'NY',
      deliveryTime: '72 hrs'
    },
  ];

  data.forEach((state, i) => {
    table.insertAdjacentHTML('beforeend', `
      <tr class="${i % 2 === 0 ? 'dark-stripe' : 'light-stripe'}">
        <td>${state.abbr}</td>
        <td>${state.deliveryTime}</td>
      </tr>
    `);
  });
});
